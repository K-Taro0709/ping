#include "Ping.h"

#include "mbed.h"

Ping::Ping(PinName PING_PIN)
        : _event(PING_PIN)
        , _cmd(PING_PIN)
        , _timer()
        , toggle()
        {
            _event.rise(mbed::Callback<void ()>(this, &Ping::_Starts));
            _event.fall(mbed::Callback<void ()>(this, &Ping::_Stops));
        }
        
void Ping::_Starts(void)
{
      _Valid = false;  // start the timere, and invalidate the current time.
      _Busy = true;
      _timer.start();
      _Time = _timer.read_us();      
}

void Ping::_Stops(void)
{
      _Valid = true;  // When it stops, update the time
      _Busy = false;
      _Time = _timer.read_us()-_Time;
      length = _Time/29/2;
}

void Ping::Send()
{
     
     _cmd.output();
     _cmd.write(0);  // see the ping documentation http://www.parallax.com/Portals/0/Downloads/docs/prod/acc/28015-PING-v1.6.pdf
    toggle.attach_us(mbed::Callback<void ()>(this, &Ping::pulse), 3);
    
}

void Ping::pulse(){
    
    static bool pinState;
    pinState = !pinState;
    
    _cmd.write(pinState);
    if(pinState){
        toggle.attach_us(mbed::Callback<void ()>(this, &Ping::pulse), 3);
    }else{
        _cmd.input();
    }
    
}

int Ping::Read_cm()
{
    return length;
}
